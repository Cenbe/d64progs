import logging
import os
from pathlib import Path
from d64py.DiskImage import DiskImage
from d64py.Constants import FileStatus
from d64py.Exceptions import PartialChainException, PartialDirectoryException

class FindScrapFiles:
    def findScrapFiles(self):
        logging.info("")
        logging.info("FindScrapFiles.py")
        logging.info("------------------")
        self.startingDir = Path("/mnt/common/download/c64/os/geos")
        logging.info(f"starting directory for recursive search: {self.startingDir}")

        filesScanned = 0; imagesScanned = 0
        scrapFiles = []
        for root, directories, files in os.walk(self.startingDir):
            filesScanned += 1
            if ".git" in root or ".svn" in root:
                continue
            for file in files:
                filesScanned += 1
                try:
                    diskImage = DiskImage(Path(root + os.sep + file))
                except Exception:
                    continue # not a disk image

                imagesScanned += 1
                try:
                    dirEntries = diskImage.getDirectory()
                except PartialDirectoryException as exc:
                    logging.error(exc)
                    logging.error(f"{diskImage.imagePath}: invalid directory!")
                    continue
                for dirEntry in dirEntries:
                    if dirEntry.getDisplayFileName() == "Photo Scrap" or dirEntry.getDisplayFileName() == "Text  Scrap":
                        print(f"{diskImage.imagePath}  {dirEntry.getDisplayFileName()}")
                        scrapFiles.append(f"{diskImage.imagePath}  {dirEntry.getDisplayFileName()}")
        print(f"{imagesScanned} images and {filesScanned} files scanned, {len(scrapFiles)} scrap files found.")
        for scrapFile in scrapFiles:
            logging.debug(scrapFile)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S',
                        filename='FindScrapFiles.log', encoding='utf-8', style='{',
                        format='{asctime} {levelname} {filename}:{lineno}: {message}')
    console = logging.StreamHandler()
    logging.getLogger().addHandler(console)
    prg = FindScrapFiles()
    prg.findScrapFiles()
