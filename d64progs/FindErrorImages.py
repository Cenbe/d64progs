import logging
import os
from pathlib import Path
from d64py.DiskImage import DiskImage
from d64py.Constants import ImageType, SectorErrors

class FindErrorImages:
    def findErrorImages(self):
        logging.info("")
        logging.info("FindErrorImages.py")
        logging.info("------------------")
        self.startingDir = Path("/mnt/common/download/c64")
        logging.info(f"starting directory for recursive search: {self.startingDir}")

        filesScanned = 0
        imagesScanned = 0
        errorImagesScanned = 0
        errorMaps = {}
        for root, directories, files in os.walk(self.startingDir):
            # store all error maps from error images
            filesScanned += 1
            if ".git" in root or ".svn" in root:
                continue
            for file in files:
                filesScanned += 1
                try:
                    diskImage = DiskImage(Path(root + os.sep + file))
                except Exception as exc:
                    continue # not a disk image

                imagesScanned += 1
                if not diskImage.imageType == ImageType.D64_ERROR:
                    diskImage.close()
                    continue
                errorImagesScanned += 1

                try:
                    errorMap = diskImage.getSectorErrorMap()
                except exc:
                    logging.exception(exc)
                    diskImage.close()
                    continue
                key = f"{root}{os.sep}{file}"
                errorMaps[key] = errorMap
                diskImage.close()

        logging.info("")
        logging.info(f"{filesScanned} files scanned")
        logging.info(f"{imagesScanned} disk images scanned")
        logging.info(f"{errorImagesScanned} error images scanned, got {len(errorMaps)} error maps")

        bogusErrors = set() # store filenames only, no dupes
        nonErrors = set()
        realErrors = set()
        for fileName in errorMaps: # key is error message
            if "COMAL0.14.D64" in fileName:
                pass
            errorMap = errorMaps[fileName]
            for key in errorMap: # key is a TrackSector
                realError = True
                # bogus errors (IP rather than FDC codes, &c.):
                if errorMap[key] not in (e.code for e in SectorErrors):
                    realError = False
                    bogusErrors.add(fileName)
                if errorMap[key] in [SectorErrors.NOT_REPORTED.code, SectorErrors.NO_ERROR.code]:
                    realError = False
                    nonErrors.add(fileName)
                if realError:
                    realErrors.add(fileName)

        logging.info("")
        logging.info("==================================================")
        logging.info(" images with BOGUS ERRORS")
        logging.info("==================================================")
        for message in bogusErrors:
            logging.info(message)

        logging.info("")
        logging.info("==================================================")
        logging.info(" images with \"no error\" or \"not reported\"")
        logging.info("==================================================")
        for message in nonErrors:
            logging.info(message)

        logging.info("")
        logging.info("==================================================")
        logging.info(" images with REAL ERRORS")
        logging.info("==================================================")
        for message in realErrors:
            logging.info(message)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S',
                        filename='/home/cenbe/d64progs.log', encoding='utf-8', style='{',
                        format='{asctime} {levelname} {filename}:{lineno}: {message}')
    console = logging.StreamHandler()
    logging.getLogger().addHandler(console)
    prg = FindErrorImages()
    prg.findErrorImages()
