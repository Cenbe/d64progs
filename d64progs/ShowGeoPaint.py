#======================================================================
# ShowGeoPaint.py
#======================================================================
from enum import Enum
import logging
import os
from pathlib import Path
import sys
from PyQt6 import QtWidgets
from PyQt6.QtCore import QSize, QPoint
from PyQt6.QtGui import QImage, QPixmap, QColor
from PyQt6.QtWidgets import QMainWindow, QHBoxLayout, QLabel, QWidget
from d64py.Constants import CharSet
from d64py.DiskImage import DiskImage
from d64py.Exceptions import InvalidRecordException

class ImageParams(Enum):
    DATA_SIZE = 1280
    COLOR_SIZE = 160
    BUFFER_SIZE = 1280 + 8 + 160 # pixels, null card, colors

class ShowGeoPaint(QMainWindow):
    # see http://zimmers.net/anonftp/pub/cbm/geos/ogramming/documents/geoPaint%20format.txt
    def __init__(self):
        super().__init__()
        # "Pepto" colors (https://www.pepto.de/projects/colorvic/2001/):
        self.peptoColors = [
            0xFF000000,
            0xFFFFFFFF,
            0xFF68372B,
            0xFF70A4B2,
            0xFF6F3D86,
            0xFF588D43,
            0xFF352879,
            0xFFB8C76F,
            0xFF6F4F25,
            0xFF433900,
            0xFF9A6759,
            0xFF444444,
            0xFF6C6C6C,
            0xFF9AD284,
            0xFF6C5EB5,
            0xFF959595
        ]
        #self.dataBytes = [0] * ImageParams.DATA_SIZE.value
        #self.colorBytes = [0] * ImageParams.COLOR_SIZE.value
        self.recordBytes = [0] * ImageParams.BUFFER_SIZE.value

        logging.basicConfig(level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S',
                            filename=str(Path.home()) + os.sep + "ShowGeoPaint.log", encoding="utf-8", style="{",
                            format="{asctime} {levelname} {filename}:{lineno}: {message}")
        console = logging.StreamHandler()
        logging.getLogger().addHandler(console)
        message = "ShowGeoPaint.py -- test image preview"
        logging.info("")
        logging.info(message)
        logging.info('-' * len(message))

        layout = QHBoxLayout()
        self.lblPreview = QLabel("")
        layout.addWidget(self.lblPreview)
        centralWidget = QWidget()
        centralWidget.setLayout(layout)
        self.setCentralWidget(centralWidget)

        #imageName = "/mnt/common/download/c64/d81-backups/GEOPAINT.D81"
        #fileName = "Calvin & Hobbes"
        #fileName = "KING TUT 1"
        #fileName = "Jimi Hendrix"
        #fileName = "jimi bare chest"
        #fileName = "jimi big hat"
        #fileName = "Robert Morris"
        #imageName = "/mnt/common/download/c64/os/geos/geos-klingon.d64"
        #fileName = "klingon ship"
        imageName = "/home/cenbe/temp/geopaint-boot.d81"
        fileName = "Lobster"
        #fileName = "test1"
        #fileName = "test2"
        #fileName = "test3"
        #fileName = "test4" # has deleted junk in records past data
        #fileName = "test5" # has junk records deleted -- not the answer

        self.card = 0; self.row = 0 # coordinates into image
        self.cardRow = 0 # two card rows per VLIR record

        try:
            diskImage = DiskImage(Path(imageName))
            dirEntry = diskImage.findDirEntry(fileName, CharSet.ASCII)
            if not dirEntry:
                raise Exception(f"can't find {fileName}!")
            logging.debug(dirEntry)
        except Exception as exc:
            logging.exception(exc)

        #======================================================================
        # Get height of image (width is always 640).
        #======================================================================
        index = diskImage.getGeosVlirIndex(dirEntry)
        record = 0; firstEmpty = 0; foundEmpty = False
        # geoPaint files have 45 records (0-44) of two card rows each,
        # or fewer if the image is shorter (width is always the same)
        while record < 45:
            offset = (record + 1) * 2  # convert VLIR record no. to sector index
            if index[offset] == 0 and index[offset + 1] == 0xff: # empty record
                if not foundEmpty:
                    firstEmpty = record; foundEmpty = True
            else:
                if foundEmpty:
                    logging.debug(f"non-empty record {record} follows empty records")
            record += 1
        if firstEmpty:
            # each record holds two card rows (16 pixels tall)
            logging.debug(f"firstEmpty is {firstEmpty}, making image {16 * firstEmpty} pixels tall")
            self.rawImage = QImage(QSize(640, 16 * firstEmpty), QImage.Format.Format_Indexed8)
        else:
            self.rawImage = QImage(QSize(640, 720), QImage.Format.Format_Indexed8)
        self.rawImage.fill(0)  # clear it

        # set up color table
        i = 0
        while i < 16:
            self.rawImage.setColor(i, self.peptoColors[i])
            i += 1

        #======================================================================
        # Decompress data and plot pixels with color data.
        #======================================================================
        record = 0
        while record < (45 if not foundEmpty else firstEmpty):
            logging.debug(f"record {record}")
            try:
                self.vlirBuffer = diskImage.readVlirRecord(record, dirEntry)
            except Exception as exc:
                logging.exception(exc)
                record += 1
                continue
            self.vlirIndex = 0; dataIndex = 0;
            # decompress pixel and color data:
            self.processRecord(self.recordBytes, dataIndex, ImageParams.BUFFER_SIZE.value)

            # plot pixels with color data for this record
            self.colorIndex = ImageParams.DATA_SIZE.value + 8 # 8 is the null card
            self.dataIndex = 0
            while self.colorIndex < ImageParams.BUFFER_SIZE.value:
                 # process pixel data for this color card
                 self.nextCard(self.recordBytes[self.colorIndex])
                 self.colorIndex += 1

            record += 1

        #self.rawImage = rawImage.scaled(QSize(48, 42)) # double size
        iconImage = QPixmap.fromImage(self.rawImage)
        self.lblPreview.setPixmap(iconImage)

    # ======================================================================
    #  Decompress a single record to the combined pixel and color buffer.
    # ======================================================================
    def processRecord(self, outBuffer, index, limit):
        """
        Decompression routine.
        :param outBuffer: Output buffer for decompressed data.
        :param index: Index into output buffer.
        :param limit: Size of output buffer.
        """
        byteCount = 0; firstTime = True
        while byteCount < limit:
            cmd = self.vlirBuffer[self.vlirIndex]
            self.vlirIndex += 1  # point to data
            if not cmd:
                logging.debug("COMMAND IS NULL BYTE")
                break

            if cmd < 64:  # next "count" bytes are data
                count = cmd
                cmdHex = format(cmd, "02x")
                logging.debug(f"at vlirIndex {hex(self.vlirIndex - 1)}, cmd ${cmdHex}: next {count} bytes are data")
                if byteCount + count > limit:
                    count = limit - byteCount
                    adjustment = cmd - count
                    logging.debug(f"count adjusted from {cmd} to {count} (adjustment of {adjustment})")
                byteCount += count
                j = 0
                while j < count:
                    outBuffer[index] = self.vlirBuffer[self.vlirIndex + j]
                    index += 1
                    j += 1
                self.vlirIndex += count  # point to next command

            elif cmd < 128:  # repeat next card (eight bytes) "count" times
                count = cmd - 64
                cmdHex = format(cmd, "02x")
                logging.debug(f"at vlirIndex {hex(self.vlirIndex - 1)}, cmd ${cmdHex}: repeat next 8-byte card {count} times")
                if byteCount + count > limit:
                    count = limit - byteCount
                    adjustment = (cmd - 64) - count
                    logging.debug(f"count adjusted from {cmd - 64} to {count} (adjustment of {adjustment})")
                byteCount += count * 8
                j = 0
                while j < count:
                    k = 0
                    while k < 8:
                        outBuffer[index] = self.vlirBuffer[self.vlirIndex + k]
                        index += 1
                        k += 1
                    j += 1
                self.vlirIndex += 8  # point to next command

            else:  # repeat next byte "count" times
                count = cmd - 128
                cmdHex = format(cmd, "02x")
                logging.debug(f"at vlirIndex {hex(self.vlirIndex - 1)}, cmd ${cmdHex}: repeat next byte {count} times")
                if byteCount + count > limit:
                    count = limit - byteCount
                    adjustment = (cmd - 128) - count
                    logging.debug(f"count adjusted from {cmd - 128} to {count} (adjustment of {adjustment})")
                byteCount += count
                j = 0
                while j < count:
                    outBuffer[index] = self.vlirBuffer[self.vlirIndex]
                    index += 1
                    j += 1
                self.vlirIndex += 1  # point to next command

    # ======================================================================
    #  Plot a single card of data.
    # ======================================================================
    def nextCard(self, colors):
        """
        Plot the 64 pixels of a card (8 rows).
        :param b: Card's color data (4 bits each foreground/background).
        :return:
        """
        fg = (colors & 0xf0) >> 4
        bg = colors & 0x0f
        i = 0
        while i < 8: # i is line (byte) counter within card
            pixelData = self.recordBytes[self.dataIndex]
            j = 0
            while j < 8: # j is bits within this card line
                mask = (1 << (7 - j))
                data = fg if pixelData & mask else bg
                try:
                    x = (self.card * 8) + j
                    y = self.row + i
                    # logging.debug(f"x: {x}, y: {y}")
                    if x >= 640 or y >= 720:
                        logging.debug(f"OUT OF RANGE, x: {x}, y: {y}")
                    # logging.debug(f"setting {hex(data)} at {x}, {y}")
                    self.rawImage.setPixel(QPoint(x, y), data)
                except Exception as exc:
                    logging.exception(exc)
                j += 1
            i +=  1
            self.dataIndex += 1

        self.card += 1
        #logging.debug(f"card is now {self.card}, color index will be {self.colorIndex + 1}")
        if self.card == 80: # end of card row
            self.cardRow += 1
            self.card = 0
            #logging.debug(f"card row is now {self.cardRow}, card is {self.card}")
        self.row = self.cardRow * 8 # top row, incremented by i
        #logging.debug(f"top row  {self.row}")

#======================================================================

app = QtWidgets.QApplication(sys.argv)
window = ShowGeoPaint()
window.show()
app.exec()
