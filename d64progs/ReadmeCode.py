from importlib import resources
from pathlib import Path
from d64py.DiskImage import DiskImage
from d64py.Constants import CharSet
from d64py import images
from d64py.TrackSector import TrackSector

class ReadmeCode:
    path = Path("images/BOGEOS2.D64")
    print(f"path: {path}")
    image = DiskImage(resources.files(images) / "BOGEOS2.D64")
    print(f"image type: {image.imageType}")
    dirEntries = image.getDirectory()
    for dirEntry in dirEntries:
        print(f"{dirEntry.getDisplayFileName()}")
        if dirEntry.isGeosFile():
            geosFileHeader = dirEntry.getGeosFileHeader()
            print(f"permanent name string: {geosFileHeader.getPermanentNameString()}")
    
    #dirEntry = image.findDirEntry("geoBrowserDocs", CharSet.ASCII)
    #pages = image.getGeoWriteFileAsLines(dirEntry)
    #for page in pages:
    #    for line in page:
    #        print(line.text)
    
    image.readBam() # cache it
    print(f"image type: {image.imageType}")
    lines = image.dumpBam(image.imageType)
    for line in lines:
        print(line)
        
    print(f"8/20 allocated? {image.isSectorAllocated(TrackSector(8, 20))}")
    print(f"9/0 allocated? {image.isSectorAllocated(TrackSector(9, 0))}")
    print(f"9/1 allocated? {image.isSectorAllocated(TrackSector(9, 1))}")
    