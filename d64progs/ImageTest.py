#======================================================================
# ImageTest.py
#======================================================================
import sys
from PyQt6.QtCore import QSize, QPoint
from PyQt6.QtGui import QImage, QPixmap, QColor
from PyQt6 import QtWidgets
from PyQt6.QtWidgets import (
    QMainWindow, QHBoxLayout, QVBoxLayout, QLabel, QWidget
)

class ImageTest(QMainWindow):
    def __init__(self):
        super().__init__()
        self.peptoColors = [
            0xFF000000,
            0xFFFFFFFF,
            0xFF68372B,
            0xFF70A4B2,
            0xFF6F3D86,
            0xFF588D43,
            0xFF352879,
            0xFFB8C76F,
            0xFF6F4F25,
            0xFF433900,
            0xFF9A6759,
            0xFF444444,
            0xFF6C6C6C,
            0xFF9AD284,
            0xFF6C5EB5,
            0xFF959595
        ]

        layout = QVBoxLayout()
        self.lblPreview = QLabel("")
        layout.addWidget(self.lblPreview)
        centralWidget = QWidget()
        centralWidget.setLayout(layout)
        self.setCentralWidget(centralWidget)

        self.rawImage = QImage(QSize(256, 256), QImage.Format.Format_Indexed8)
        #self.rawImage = QImage(QSize(256, 256), QImage.Format.Format_RGBX8888)
        self.rawImage.fill(0)

        # set up color table
        i = 0
        while i < 16:
            self.rawImage.setColor(i, self.peptoColors[i])
            i += 1
        print(f"color count: {self.rawImage.colorCount()}")
        for color in self.rawImage.colorTable():
            print(hex(color))

        # put another square in the upper-left corner
        x = 0
        while x < 64:
            y = 0
            while y < 64:
                print(f"x: {x}, y: {y}")
                self.rawImage.setPixel(QPoint(x, y), 5)
                y += 1
            x += 1
        image = QPixmap.fromImage(self.rawImage)
        self.lblPreview.setPixmap(image)

#======================================================================

app = QtWidgets.QApplication(sys.argv)
window = ImageTest()
window.show()
app.exec()
