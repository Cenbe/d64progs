class TestBaseException(BaseException):
    def __init__(self, message):
        super().__init__(message)

class TestException(Exception):
    def __init__(self, message):
        super().__init__(message)

class ExceptionTest():
    def doTest(self):
        try:
            self.raiseException()
        except Exception as exc:
            print(f"caught TestException: {exc}")

        try:
            self.raiseBaseException()
        except Exception as exc:
            print(f"caught TestBaseException: {exc}")

    def raiseException(self):
        raise TestException("I'm a TestException")

    def raiseBaseException(self):
        raise TestBaseException("I'm a TestBaseException")


if __name__ == '__main__':
    prg = ExceptionTest()
    prg.doTest()
