import logging
import os
from pathlib import Path
from d64py.DiskImage import DiskImage
#from d64py.Constants import ImageType, SectorErrors
from d64py.Exceptions import PartialChainException, PartialDirectoryException

class FindManagerFiles:
    def findManagerFiles(self):
        logging.info("")
        logging.info("FindManagerFiles.py")
        logging.info("------------------")
        self.startingDir = Path("/mnt/common/download/c64")
        logging.info(f"starting directory for recursive search: {self.startingDir}")

        filesScanned = 0; imagesScanned = 0
        textFiles = []; photoFiles = []
        for root, directories, files in os.walk(self.startingDir):
            filesScanned += 1
            if ".git" in root or ".svn" in root:
                continue
            for file in files:
                filesScanned += 1
                try:
                    diskImage = DiskImage(Path(root + os.sep + file))
                except Exception as exc:
                    continue # not a disk image

                imagesScanned += 1
                try:
                    dirEntries = diskImage.getDirectory()
                except PartialDirectoryException as exc:
                    logging.error(exc)
                    logging.error(f"{diskImage.imagePath}: invalid directory!")
                    continue
                for dirEntry in dirEntries:
                    if dirEntry.isGeosFile():
                        if dirEntry.geosFileHeader is None: 
                            logging.error(f"{diskImage.imagePath}, file {dirEntry.getDisplayFileName()} is GEOS but has no header!")
                            continue
                        try:
                            if dirEntry.geosFileHeader.getPermanentNameString().startswith("text album"):
                                textFiles.append(f"{diskImage.imagePath}  {dirEntry.getDisplayFileName()}  {dirEntry.geosFileHeader.getPermanentNameString()}")
                            if dirEntry.geosFileHeader.getPermanentNameString().startswith("photo album"):
                                photoFiles.append(f"{diskImage.imagePath}  {dirEntry.getDisplayFileName()}  {dirEntry.geosFileHeader.getPermanentNameString()}")
                        except PartialChainException as exc:
                            logging.exception(exc)
                            continue

        for textFile in textFiles:
            logging.debug(textFile)
        logging.debug("")
        for photoFile in photoFiles:
            logging.debug(photoFile)

if __name__ == '__main__':
    homedir = os.path.expanduser( '~' )
    logging.basicConfig(level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S',
                        filename='/home/cenbe/FindManagerFiles.log', encoding='utf-8', style='{',
                        format='{asctime} {levelname} {filename}:{lineno}: {message}')
    console = logging.StreamHandler()
    logging.getLogger().addHandler(console)
    prg = FindManagerFiles()
    prg.findManagerFiles()
