from importlib import resources
from d64py.Constants import CharSet
from d64py.DiskImage import DiskImage
from d64py import D64Utility
from d64py import images

if __name__ == '__main__':
    image = DiskImage(resources.files(images) / "BOGEOS2.D64")
    dirEntry = image.findDirEntry("browserdocs.cbm",CharSet.PETSCII)
    lines = image.getFileAsText(dirEntry, CharSet.PETSCII, False)

    with open("/home/cenbe/temp/browserdocs", "w") as f:
        for line in lines:
            f.write(line.text)
    f.close()
    # with open(file_1) as f1, open(file_2) as f2
    with open("/home/cenbe/temp/browserdocs.conv", "w") as o:
        for line in lines:
            bytes = bytearray()
            bytes.extend(bytearray(line.text))
            for byte in bytes:
                f.write(D64Utility.petsciiToAsciiChar(byte))
    f.close()
