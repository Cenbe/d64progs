import logging
import os
from pathlib import Path
from d64py.DiskImage import DiskImage
from d64py.Constants import FileStatus
from d64py.Exceptions import PartialChainException, PartialDirectoryException

class FindLockedFiles:
    def findLockedFiles(self):
        logging.info("")
        logging.info("FindLockedFiles.py")
        logging.info("------------------")
        self.startingDir = Path("/mnt/common/download/c64")
        logging.info(f"starting directory for recursive search: {self.startingDir}")

        filesScanned = 0; imagesScanned = 0
        lockedFiles = []
        for root, directories, files in os.walk(self.startingDir):
            filesScanned += 1
            if ".git" in root or ".svn" in root:
                continue
            for file in files:
                filesScanned += 1
                try:
                    diskImage = DiskImage(Path(root + os.sep + file))
                except Exception:
                    continue # not a disk image

                imagesScanned += 1
                try:
                    dirEntries = diskImage.getDirectory()
                except PartialDirectoryException as exc:
                    logging.error(exc)
                    logging.error(f"{diskImage.imagePath}: invalid directory!")
                    continue
                for dirEntry in dirEntries:
                    print(f"file status: {'0x%0*x' % (2, dirEntry.getFileStatus())}")
                    if dirEntry.getFileStatus() == FileStatus.FILE_LOCKED.value:
                        print(f"{diskImage.imagePath}  {dirEntry.getDisplayFileName()}")
                        lockedFiles.append(f"{diskImage.imagePath}  {dirEntry.getDisplayFileName()}")
        print(f"{imagesScanned} images and {filesScanned} files scanned, {len(lockedFiles)} locked files found.")
        for lockedFile in lockedFiles:
            logging.debug(lockedFile)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S',
                        filename='FindLockedFiles.log', encoding='utf-8', style='{',
                        format='{asctime} {levelname} {filename}:{lineno}: {message}')
    console = logging.StreamHandler()
    logging.getLogger().addHandler(console)
    prg = FindLockedFiles()
    prg.findLockedFiles()
